# Module Cognito

Creates AWS Cognito that works with `SAML` as an identity provider.

## Workflow

Before the first run, there is no value for the `saml_metadata_document` variable. \
After the first run, data is obtained from the output, which is entered into the selected IDP (eg Azure), from which an XML file is generated, which serves as the data stage for the variable `saml_metadata_document`. \
Then it is necessary to start the module once more, which will lead to full setup and commissioning.

To create a `Cognito identity pool` it is necessary to enter the value of the variables ʻidentity_pool_name_X`,` saml_metadata_document` and `saml_provider_name`

## Note
All `* _name` variables can only contain letters, numbers, or" - ", but ʻidentity_pool_name_X` can only contain letters, numbers, and spaces. \
For unnecessary clients or identity pools, define their value as ""

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| project_and_env | Name of the project and environment | `string` | n/a | no |
| aws_region | Name of AWS region. | `string` | n/a | yes |
| suffix | Suffix of name. | `string` | n/a | no |
| generate_secret | Should be an application secret be generated. | `bool` | true | no |
| saml_metadata_document | Path to the document with SAML metadata. | `string` | n/a | yes |
| tags_user_pool | A mapping of tags to assign to User Pool. | `map` | n/a | no |
| user_pool_nam | The name of the user pool. | `string` | n/a | yes |
| username_attributes | Specifies whether email addresses of phone numbers can be specified as usernames when a user signs up. Conflicts with alias_attributes. | `list` | n/a | no |
| auto_verified_attributes | The attribute to be auto-verified. Possible values: email, phone_number. | `list` | n/a | no |
| allow_admin_create_user_only | Set to true if only the administrator is allowed to create user profiles. Set false if users can sign themselves up via an app. | `bool` | true | no |
| minimum_length | The minimum length of the password policy that you have set. | `number` | 8 | no |
| require_lowercase | Whether you have required users to use at least one lowercase letter in their password. | `bool` | false | no |
| require_numbers | Whether you have required users to use at least one number in their password. | `bool` | false | no |
| require_symbols | Whether you have required users to use at least on symbol in their password. | `bool` | false | no |
| require_uppercase | Whether you have required users to use at least on symbol in their password. | `bool` | false | no |
| temporary_password_validity_days | The user account expiration limit, in days, after which the account is no longer usable. | `number` | 7 | no |
| schema_name_1 | Name of schema number 1. | `string` | n/a | no |
| schema_attritude_data_type_1 | The attribute data type. Must be one of Boolean, Number, String, DateTime. | `string` | n/a | no |
| schema_developer_only_attribute_1 | Specifies whether the attribute type is developer only. Must be one of True, False. | `bool` | n/a | no |
| schema_mutable_1 | Specifies whether the attribute can be changed once it has been created. Must be one of True, False. | `bool` | n/a | no |
| schema_required_1 | Specifies whether a user pool attribute is required. If the attribute is required and the user does not provide a value, registration or sign-in will fail.  Must be one of True, False. | `bool` | n/a | no |
| schema_string_attribute_constraints_min_length_1 | The minimum length of an attribute value of the string type. | `number` | 0 | no |
| schema_string_attribute_constraints_max_length_1 | The maximum length of an attribute value of the string type. | `number` | 2048 | no |
| client_name_1 | The name of the application client. | `string` | n/a | yes |
| default_redirect_uri_1 | The default redirect URI. Must be in the list of callback URLs.. | `string` | n/a | no |
| callback_urls_1 | List of allowed callback URLs for the identity providers. | `list` | n/a | no |
| logout_urls_1 | List of allowed logout URLs for the identity providers. | `list` | n/a | no |
| client_name_2 | The name of the application client. | `string` | n/a | yes |
| default_redirect_uri_2 | The default redirect URI. Must be in the list of callback URLs.. | `string` | n/a | no |
| callback_urls_2 | List of allowed callback URLs for the identity providers. | `list` | n/a | no |
| logout_urls_2 | List of allowed logout URLs for the identity providers. | `list` | n/a | no 
| client_name_3 | The name of the application client. | `string` | n/a | yes |
| default_redirect_uri_3 | The default redirect URI. Must be in the list of callback URLs.. | `string` | n/a | no |
| callback_urls_3 | List of allowed callback URLs for the identity providers. | `list` | n/a | no |
| logout_urls_3 | List of allowed logout URLs for the identity providers. | `list` | n/a | no 
| client_name_4 | The name of the application client. | `string` | n/a | yes |
| default_redirect_uri_4 | The default redirect URI. Must be in the list of callback URLs.. | `string` | n/a | no |
| callback_urls_4 | List of allowed callback URLs for the identity providers. | `list` | n/a | no |
| logout_urls_4 | List of allowed logout URLs for the identity providers. | `list` | n/a | no 
| client_name_5 | The name of the application client. | `string` | n/a | yes |
| default_redirect_uri_5 | The default redirect URI. Must be in the list of callback URLs.. | `string` | n/a | no |
| callback_urls_5 | List of allowed callback URLs for the identity providers. | `list` | n/a | no |
| logout_urls_5 | List of allowed logout URLs for the identity providers. | `list` | n/a | no 
| explicit_auth_flows | List of authentication flows (ADMIN_NO_SRP_AUTH, CUSTOM_AUTH_FLOW_ONLY, USER_PASSWORD_AUTH). | `list` | n/a | no |
| read_attributes | List of user pool attributes the application client can read from. | `list` | n/a | no |
| write_attributes | List of user pool attributes the application client can write to. | `list` | n/a | no |
| allowed_oauth_flows | List of allowed OAuth flows (code, implicit, client_credential). | `list` | n/a | no |
| refresh_token_validity | The time limit in days refresh tokens are valid for. | `number` | n/a | no |
| allowed_oauth_flows_user_pool_client | Whether the client is allowed to follow the OAuth protocol whe interacting with Cognito User Pool (true or false). | `bool` | true | no |
| allowed_oauth_scopes | List of allowed OAuth scopes (phone, email, openid, profile, and aws.cognito, sign-in.user.admin). | `list` | n/a | no |
| domain_name | The domain name. | `string` | n/a | yes |
| provider_name | The provider name. | `string` | n/a | yes |
| provider_type | The provider type. | `string` | SAML | yes |
| attribute_mapping | The map of attribute mapping of user pool attributes. | `map` | name, given_name, family_name, email | no |

* If Cognito identity pool(s) is(are) needed

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| saml_provider_name | Name of SAML provider. | `string` | n/a | yes |
| identity_pool_name_1 | The cognito identity pool name. | `string` | n/a | yes |
| identity_pool_name_2 | The cognito identity pool name. | `string` | n/a | yes |
| identity_pool_name_3 | The cognito identity pool name. | `string` | n/a | yes |
| identity_pool_name_4 | The cognito identity pool name. | `string` | n/a | yes |
| identity_pool_name_5 | The cognito identity pool name. | `string` | n/a | yes |
| server_side_token_check | Whether server-side token validation is enabled for the identity provider’s token or not.(true or false). | `bool` | false | no |
| allow_unauthenticated_identities | Whether the identity pool supports unauthenticated logins or not. | `bool` | false | yes |
| project_and_env_identity_pool_1 | Name of the project and environment (must contain only alphanumeric characters and spaces). | `sering` | n/a | false |
| project_and_env_identity_pool_2 | Name of the project and environment (must contain only alphanumeric characters and spaces). | `sering` | n/a | false |
| project_and_env_identity_pool_3 | Name of the project and environment (must contain only alphanumeric characters and spaces). | `sering` | n/a | false |
| project_and_env_identity_pool_4 | Name of the project and environment (must contain only alphanumeric characters and spaces). | `sering` | n/a | false |
| project_and_env_identity_pool_5 | Name of the project and environment (must contain only alphanumeric characters and spaces). | `sering` | n/a | false |
| tags_user_identity_pool_1 | A mapping of tags to assign to Identity pool 1. | `map` | n/a | no |
| tags_user_identity_pool_2 | A mapping of tags to assign to Identity pool 1. | `map` | n/a | no |
| tags_user_identity_pool_3 | A mapping of tags to assign to Identity pool 1. | `map` | n/a | no |
| tags_user_identity_pool_4 | A mapping of tags to assign to Identity pool 1. | `map` | n/a | no |
| tags_user_identity_pool_5 | A mapping of tags to assign to Identity pool 1. | `map` | n/a | no |

## Outputs

| Name | Description |
|------|-------------|
| user_pool_id | The id of the user pool. |
| user_pool_arn | The ARN of the user pool. |
| user_pool_endpoint | The endpoint name of the user pool. Example format: cognito-idp.REGION.amazonaws.com/xxxx_yyyyy. |
| user_pool_client_id_X | The id of the user pool client. |
| user_pool_client_secrets_X | The client secrets of the user pool clients. |
| provider_name | Identity Provider Client Name. |
