provider "aws" {
  region = "eu-central-1"
}


module "cognito" {
  source = "../../"
  client_name_1 = "pricing-dev"
  client_name_2 = "pricing-local"
  client_name_3 = ""
  client_name_4 = ""
  client_name_5 = ""
  aws_region = "eu-central-1"
  user_pool_name = "neeco"
  domain_name = "neeco"
  provider_name = "neeco-azure"
  identity_pool_name_1 = ""
  identity_pool_name_2 = ""
  identity_pool_name_3 = ""
  identity_pool_name_4 = ""
  identity_pool_name_5 = ""
  saml_provider_name = ""
  auto_verified_attributes = ["email"]
  username_attributes = ["email"]
  provider_type = "SAML"
  refresh_token_validity = 14
  saml_metadata_document = "test.xml"
  schema_name_1 = "email"
  schema_attribute_data_type_1 = "String"
  schema_developer_only_attribute_1 = "false"
  schema_mutable_1 = "true"
  schema_required_1 = "true"
  schema_string_attribute_constraints_min_length_1 = 0
  schema_string_attribute_constraints_max_length_1 = 2048
  allowed_oauth_flows_user_pool_client = "true"
  allowed_oauth_flows = ["code","implicit"]
  allowed_oauth_scopes = ["email", "openid", "profile", "aws.cognito.signin.user.admin"]
  project_and_env = ""
  project_and_env_identity_pool_1 = ""
  project_and_env_identity_pool_2 = ""
  project_and_env_identity_pool_3 = ""
  project_and_env_identity_pool_4 = ""
  project_and_env_identity_pool_5 = ""
  suffix = ""
  callback_urls_1 = ["https://asd.com/login", "https://asd.cloud/login", "http://localhost:8080/login"]
  callback_urls_2 = ["https://qwe.com/login", "https://qwe.cloud/login", "http://localhost:9090/login"]
  callback_urls_3 = []
  callback_urls_4 = []
  callback_urls_5 = []
  logout_urls_1 = ["https://asd.com/logout", "https://asd.cloud/logout"]
  logout_urls_2 = ["https://qwe.com/logout", "https://qwe.cloud/logout"]
  logout_urls_3 = []
  logout_urls_4 = []
  logout_urls_5 = []
  default_redirect_uri_1 = "http://localhost:8080/login"
  default_redirect_uri_2 = "http://localhost:9090/login"
  default_redirect_uri_3 = ""
  default_redirect_uri_4 = ""
  default_redirect_uri_5 = ""
  tags_user_pool = {ENV = "infra", APP = "test"}
  tags_identity_pool_1 = {}
  tags_identity_pool_2 = {}
  tags_identity_pool_3 = {}
  tags_identity_pool_4 = {}
  tags_identity_pool_5 = {}
}