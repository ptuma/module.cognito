resource "aws_iam_saml_provider" "default" {
  count                                =  var.identity_pool_name_1 != "" ? 1 : 0
  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.saml_provider_name}"
  saml_metadata_document               = file(var.saml_metadata_document)
}
resource "aws_cognito_identity_pool" "main" {
  count                                 =  var.identity_pool_name_1 != "" ? 1 : 0
  identity_pool_name                    = "${var.project_and_env_identity_pool_1 == "" ? "" : "${var.project_and_env_identity_pool_1} "}${var.identity_pool_name_1}"
  allow_unauthenticated_identities = var.allow_unauthenticated_identities

  cognito_identity_providers {
    client_id                           = aws_cognito_user_pool_client.client1a.0.id #default na client1
    provider_name                       = "cognito-idp.${var.aws_region}.amazonaws.com/${aws_cognito_user_pool.pool.id}"
    server_side_token_check = var.server_side_token_check
  }

tags                                    = var.tags_identity_pool_1

}

resource "aws_cognito_identity_pool" "ip2" {
  count                                 =  var.identity_pool_name_2 != "" ? 1 : 0
  identity_pool_name                    = "${var.project_and_env_identity_pool_2 == "" ? "" : "${var.project_and_env_identity_pool_2} "}${var.identity_pool_name_2}"
  allow_unauthenticated_identities = var.allow_unauthenticated_identities

  cognito_identity_providers {
    client_id                           = aws_cognito_user_pool_client.client2a.0.id
    provider_name                       = "cognito-idp.${var.aws_region}.amazonaws.com/${aws_cognito_user_pool.pool.id}"
    server_side_token_check = var.server_side_token_check
  }

tags                                    = var.tags_identity_pool_2

}
resource "aws_cognito_identity_pool" "ip3" {
  count                                 =  var.identity_pool_name_3 != "" ? 1 : 0
  identity_pool_name                    = "${var.project_and_env_identity_pool_3 == "" ? "" : "${var.project_and_env_identity_pool_3} "}${var.identity_pool_name_3}"
  allow_unauthenticated_identities = var.allow_unauthenticated_identities

  cognito_identity_providers {
    client_id                           = aws_cognito_user_pool_client.client3a.0.id
    provider_name                       = "cognito-idp.${var.aws_region}.amazonaws.com/${aws_cognito_user_pool.pool.id}"
    server_side_token_check = var.server_side_token_check
  }

tags                                    = var.tags_identity_pool_3

}
resource "aws_cognito_identity_pool" "ip4" {
  count                                 =  var.identity_pool_name_4 != "" ? 1 : 0
  identity_pool_name                    = "${var.project_and_env_identity_pool_4 == "" ? "" : "${var.project_and_env_identity_pool_4} "}${var.identity_pool_name_4}"
  allow_unauthenticated_identities = var.allow_unauthenticated_identities

  cognito_identity_providers {
    client_id                           = aws_cognito_user_pool_client.client4a.0.id
    provider_name                       = "cognito-idp.${var.aws_region}.amazonaws.com/${aws_cognito_user_pool.pool.id}"
    server_side_token_check = var.server_side_token_check
  }

tags                                    = var.tags_identity_pool_4

}
resource "aws_cognito_identity_pool" "ip5" {
  count                                 =  var.identity_pool_name_5 != "" ? 1 : 0
  identity_pool_name                    = "${var.project_and_env_identity_pool_5 == "" ? "" : "${var.project_and_env_identity_pool_5} "}${var.identity_pool_name_5}"
  allow_unauthenticated_identities = var.allow_unauthenticated_identities

  cognito_identity_providers {
    client_id                           = aws_cognito_user_pool_client.client5a.0.id
    provider_name                       = "cognito-idp.${var.aws_region}.amazonaws.com/${aws_cognito_user_pool.pool.id}"
    server_side_token_check = var.server_side_token_check
  }

tags                                    = var.tags_identity_pool_5

}