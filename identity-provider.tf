resource "aws_cognito_identity_provider" "provider" {
  count                                = var.saml_metadata_document != "" ? 1 : 0
  user_pool_id                         = aws_cognito_user_pool.pool.id
  provider_name                        = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.provider_name}"
  provider_type                        = var.provider_type
  attribute_mapping                    = var.attribute_mapping

  provider_details = {
    MetadataFile    = file(var.saml_metadata_document)
  }
}
