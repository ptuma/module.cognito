resource "aws_cognito_user_pool" "pool" {
  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.user_pool_name}"
  username_attributes                  = var.username_attributes
  auto_verified_attributes             = var.auto_verified_attributes
 
  password_policy {
      minimum_length                   = var.minimum_length
      require_lowercase                = var.require_lowercase
      require_numbers                  = var.require_numbers
      require_symbols                  = var.require_symbols
      require_uppercase                = var.require_uppercase
      temporary_password_validity_days = var.temporary_password_validity_days
  }                                       

  tags                                 = var.tags_user_pool

  admin_create_user_config {
    allow_admin_create_user_only = var.allow_admin_create_user_only
  }

  schema {
    name                     = var.schema_name_1
    attribute_data_type      = var.schema_attribute_data_type_1
    developer_only_attribute = var.schema_developer_only_attribute_1
    mutable                  = var.schema_mutable_1
    required                 = var.schema_required_1
    string_attribute_constraints {
      min_length             = var.schema_string_attribute_constraints_min_length_1
      max_length             = var.schema_string_attribute_constraints_max_length_1
    }
  }

}