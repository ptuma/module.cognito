output "user_pool_id" {
  description = "The id of the user pool"
  value       = aws_cognito_user_pool.pool.id
}

output "user_pool_arn" {
  description = "The ARN of the user pool."
  value       = aws_cognito_user_pool.pool.arn
}

output "user_pool_endpoint" {
  description = "The endpoint name of the user pool. Example format: cognito-idp.REGION.amazonaws.com/xxxx_yyyyy"
  value       = aws_cognito_user_pool.pool.endpoint
}
output "user_pool_client_id_1a" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client1a.*.id
}
output "user_pool_client_secrets_1a" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client1a.*.client_secret
}

output "user_pool_client_id_1b" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client1b.*.id
}
output "user_pool_client_secrets_1b" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client1b.*.client_secret
}

output "user_pool_client_id_2a" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client2a.*.id
}
output "user_pool_client_secrets_2a" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client2a.*.client_secret
}

output "user_pool_client_id_2b" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client2b.*.id
}
output "user_pool_client_secrets_2b" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client2b.*.client_secret
}

output "user_pool_client_id_3a" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client3a.*.id
}
output "user_pool_client_secrets_3a" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client3a.*.client_secret
}
output "user_pool_client_id_3b" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client3b.*.id
}
output "user_pool_client_secrets_3b" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client3b.*.client_secret
}
output "user_pool_client_id_4a" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client4a.*.id
}
output "user_pool_client_secrets_4a" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client4a.*.client_secret
}
output "user_pool_client_id_4b" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client4b.*.id
}
output "user_pool_client_secrets_4b" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client4b.*.client_secret
}
output "user_pool_client_id_5a" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client5a.*.id
}
output "user_pool_client_secrets_5a" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client5a.*.client_secret
}
output "user_pool_client_id_5b" {
  description = "The id of the user pool client"
  value       = aws_cognito_user_pool_client.client5b.*.id
}
output "user_pool_client_secrets_5b" {
  description = " The client secrets of the user pool clients"
  value       = aws_cognito_user_pool_client.client5b.*.client_secret
}
### OUTPUTS for identity-provider ###

output "provider_name" {
    description = "Identity Provider Client Name"
    value       = aws_cognito_identity_provider.provider.*.provider_name
}

