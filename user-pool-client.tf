resource "aws_cognito_user_pool_client" "client1a" {
  count                                = var.saml_metadata_document != "" ? 1 : 0

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_1}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = [var.provider_name, "COGNITO"]
  callback_urls                        = var.callback_urls_1
  logout_urls                          = var.logout_urls_1
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_1

  depends_on = [
        aws_cognito_identity_provider.provider
    ]
}

resource "aws_cognito_user_pool_client" "client1b" {
  count                                = var.saml_metadata_document != "" ? 0 : 1

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_1}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = var.callback_urls_1
  logout_urls                          = var.logout_urls_1
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_1
}

resource "aws_cognito_user_pool_client" "client2a" {
  count                                 =  (var.client_name_2 != "" && var.saml_metadata_document != "") ? 1 : 0

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_2}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = [var.provider_name, "COGNITO"]
  callback_urls                        = var.callback_urls_2
  logout_urls                          = var.logout_urls_2
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_2

  depends_on = [
      aws_cognito_identity_provider.provider
  ]

}

resource "aws_cognito_user_pool_client" "client2b" {
  count                                 =  (var.client_name_2 != "" && var.saml_metadata_document != "") ? 0 : 1

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_2}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = var.callback_urls_2
  logout_urls                          = var.logout_urls_2
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_2
}

resource "aws_cognito_user_pool_client" "client3a" {
  count                                 =  (var.client_name_3 != "" && var.saml_metadata_document != "") ? 1 : 0

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_3}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = [var.provider_name, "COGNITO"]
  callback_urls                        = var.callback_urls_3
  logout_urls                          = var.logout_urls_3
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_3

  depends_on = [
      aws_cognito_identity_provider.provider
  ]

}

resource "aws_cognito_user_pool_client" "client3b" {
  count                                 =  (var.client_name_3 != "" && var.saml_metadata_document != "") ? 0 : 1

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_3}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = var.callback_urls_3
  logout_urls                          = var.logout_urls_3
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_3
}

resource "aws_cognito_user_pool_client" "client4a" {
  count                                 =  (var.client_name_4 != "" && var.saml_metadata_document != "") ? 1 : 0

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_4}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = [var.provider_name, "COGNITO"]
  callback_urls                        = var.callback_urls_4
  logout_urls                          = var.logout_urls_4
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_4

  depends_on = [
      aws_cognito_identity_provider.provider
  ]

}

resource "aws_cognito_user_pool_client" "client4b" {
  count                                 =  (var.client_name_4 != "" && var.saml_metadata_document != "") ? 0 : 1

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_4}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = var.callback_urls_4
  logout_urls                          = var.logout_urls_4
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_4
}

resource "aws_cognito_user_pool_client" "client5a" {
  count                                 =  (var.client_name_5 != "" && var.saml_metadata_document != "") ? 1 : 0

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_5}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = [var.provider_name, "COGNITO"]
  callback_urls                        = var.callback_urls_5
  logout_urls                          = var.logout_urls_5
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_5

  depends_on = [
      aws_cognito_identity_provider.provider
  ]

}

resource "aws_cognito_user_pool_client" "client5b" {
  count                                 =  (var.client_name_5 != "" && var.saml_metadata_document != "") ? 0 : 1

  name                                 = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.client_name_5}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  generate_secret                      = var.generate_secret
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = var.callback_urls_5
  logout_urls                          = var.logout_urls_5
  allowed_oauth_flows                  = var.allowed_oauth_flows
  refresh_token_validity               = var.refresh_token_validity
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  allowed_oauth_scopes                 = var.allowed_oauth_scopes
  default_redirect_uri                 = var.default_redirect_uri_5
}