resource "aws_cognito_user_pool_domain" "main" {
  domain                                = var.suffix == "" ? var.domain_name : "${var.domain_name}-${var.suffix}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
}
