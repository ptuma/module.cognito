## General variables ##

variable "aws_region" {
  description = "Name of AWS region"
  type = string
  default = ""
}

variable "project_and_env" {
  description = "Name of the project and environment"
  type        = string
  default     = ""
}

variable "project_and_env_identity_pool_1" {
  description = "Name of the project and environment (must contain only alphanumeric characters and spaces)"
  type        = string
  default     = ""
}

variable "project_and_env_identity_pool_2" {
  description = "Name of the project and environment (must contain only alphanumeric characters and spaces)"
  type        = string
  default     = ""
}

variable "project_and_env_identity_pool_3" {
  description = "Name of the project and environment (must contain only alphanumeric characters and spaces)"
  type        = string
  default     = ""
}

variable "project_and_env_identity_pool_4" {
  description = "Name of the project and environment (must contain only alphanumeric characters and spaces)"
  type        = string
  default     = ""
}

variable "project_and_env_identity_pool_5" {
  description = "Name of the project and environment (must contain only alphanumeric characters and spaces)"
  type        = string
  default     = ""
}

variable "suffix"{
    description = "Suffix"
    type        =  string
    default     =  ""

}

variable "generate_secret" {
  type        = bool
  description = "Should be an application secret be generated (true or false)"
  default     = true
}

variable "saml_metadata_document" {
  type        = string
  description = "Path to the document with SAML metadata"
  default = ""
}

variable "tags_user_pool" {
  type        = map
  description = "A mapping of tags to assign to User Pool"
  default     = {}
}

variable "tags_identity_pool_1" {
  type        = map
  description = "A mapping of tags to assign to and Identity Pool 1"
  default     = {}
}

variable "tags_identity_pool_2" {
  type        = map
  description = "A mapping of tags to assign to and Identity Pool 2"
  default     = {}
}

variable "tags_identity_pool_3" {
  type        = map
  description = "A mapping of tags to assign to and Identity Pool 3"
  default     = {}
}

variable "tags_identity_pool_4" {
  type        = map
  description = "A mapping of tags to assign to and Identity Pool 4"
  default     = {}
}

variable "tags_identity_pool_5" {
  type        = map
  description = "A mapping of tags to assign to and Identity Pool 5"
  default     = {}
}

## user_pool variables ##

variable "user_pool_name" {
  type        = string
  description = "The name of the user pool"
}

variable "username_attributes" {
  type        = list(string)
  description = "Specifies whether email addresses of phone numbers can be specified as usernames when a user signs up. Conflicts with alias_attributes "
  default     = null
}

variable "auto_verified_attributes" {
  type        = list(string)
  description = "The attribute to be auto-verified. Possible values: email, phone_number"
  default     = null
}

variable "allow_admin_create_user_only" {
  type        = bool
  description = "Set to true if only the administrator is allowed to create user profiles. Set false if users can sign themselves up via an app."
  default     = true
}

variable "password_policy" {
  description = "A container for information about the user pool password policy"
  type = object({
    minimum_length                   = number,
    require_lowercase                = bool,
    require_lowercase                = bool,
    require_numbers                  = bool,
    require_symbols                  = bool,
    require_uppercase                = bool,
    temporary_password_validity_days = number
  })
  default = null
}

variable "minimum_length" {
  type        = number
  description = "The minimum length of the password policy that you have set"
  default     = "8"
}

variable "require_lowercase" {
  type        = bool
  description = "Whether you have required users to use at least one lowercase letter in their password"
  default     = false
}

variable "require_numbers" {
  type        = bool
  description = "Whether you have required users to use at least one number in their password"
  default     = false
}

variable "require_symbols" {
  type        = bool
  description = "Whether you have required users to use at least on symbol in their password"
  default     = false
}

variable "require_uppercase" {
  type        = bool
  description = "Whether you have required users to use at least one uppercase letter in their password"
  default     = false
}

variable "temporary_password_validity_days" {
  type        = number
  description = "The user account expiration limit, in days, after which the account is no longer usable"
  default     = 7
}

variable "schema_name_1" {
  type        = string
  description = "(Required) - Name of schema number 1"
  default = ""
}

variable "schema_attribute_data_type_1" {
  type        = string
  description = "(Required) - The attribute data type. Must be one of Boolean, Number, String, DateTime."
  default = ""
}

variable "schema_developer_only_attribute_1" {
  type        = bool
  description = "(Optional) - Specifies whether the attribute type is developer only. Must be one of True, False"

}

variable "schema_mutable_1" {
  type        = bool
  description = "(Optional) - Specifies whether the attribute can be changed once it has been created. Must be one of True, False"

}

variable "schema_required_1" {
  type        = bool
  description = "(Optional) - Specifies whether a user pool attribute is required. If the attribute is required and the user does not provide a value, registration or sign-in will fail.  Must be one of True, False"

}

variable "schema_string_attribute_constraints_min_length_1" {
  type        = number
  description = "(Optional) - The minimum length of an attribute value of the string type."
  default = "0"
}

variable "schema_string_attribute_constraints_max_length_1" {
  type        = number
  description = "(Optional) - The maximum length of an attribute value of the string type."
  default = "2048"
}

## user_pool_client variables ##

variable "client_name_1" {
  type        = string
  description = "The name of the application client1"
}

variable "client_name_2" {
  type        = string
  description = "The name of the application client2"
}

variable "client_name_3" {
  type        = string
  description = "The name of the application client2"
}

variable "client_name_4" {
  type        = string
  description = "The name of the application client2"
}

variable "client_name_5" {
  type        = string
  description = "The name of the application client2"
}

variable "explicit_auth_flows" {
  type         = list(string)
  description = "List of authentication flows (ADMIN_NO_SRP_AUTH, CUSTOM_AUTH_FLOW_ONLY, USER_PASSWORD_AUTH)"
  default      = []
}

variable "read_attributes" {
  type        = list(string)
  description = "List of user pool attributes the application client can read from"
  default     = []
}

variable "write_attributes" {
  type        = list(string)
  description = "List of user pool attributes the application client can write to"
  default     = []
}

variable "callback_urls_1" {
  type        = list
  description = "List of allowed callback URLs for the identity providers"
  default     = []
}

variable "logout_urls_1" {
  type        = list
  description = "List of allowed logout URLs for the identity providers"
  default     = []
}

variable "default_redirect_uri_1" {
  type        = string
  description = "The default redirect URI. Must be in the list of callback URLs."
}

variable "callback_urls_2" {
  type        = list
  description = "List of allowed callback URLs for the identity providers"
  default     = []
}

variable "logout_urls_2" {
  type        = list
  description = "List of allowed logout URLs for the identity providers"
  default     = []
}

variable "default_redirect_uri_2" {
  type        = string
  description = "The default redirect URI. Must be in the list of callback URLs."
}

variable "callback_urls_3" {
  type        = list
  description = "List of allowed callback URLs for the identity providers"
  default     = []
}

variable "logout_urls_3" {
  type        = list
  description = "List of allowed logout URLs for the identity providers"
  default     = []
}

variable "default_redirect_uri_3" {
  type        = string
  description = "The default redirect URI. Must be in the list of callback URLs."
}

variable "callback_urls_4" {
  type        = list
  description = "List of allowed callback URLs for the identity providers"
  default     = []
}

variable "logout_urls_4" {
  type        = list
  description = "List of allowed logout URLs for the identity providers"
  default     = []
}

variable "default_redirect_uri_4" {
  type        = string
  description = "The default redirect URI. Must be in the list of callback URLs."
}

variable "callback_urls_5" {
  type        = list
  description = "List of allowed callback URLs for the identity providers"
  default     = []
}

variable "logout_urls_5" {
  type        = list
  description = "List of allowed logout URLs for the identity providers"
  default     = []
}

variable "default_redirect_uri_5" {
  type        = string
  description = "The default redirect URI. Must be in the list of callback URLs."
}
variable "allowed_oauth_flows" {
  type        = list(string)
  description = "List of allowed OAuth flows (code, implicit, client_credential)"
  default     = []
}

variable "refresh_token_validity" {
  type        = number
  description = "The time limit in days refresh tokens are valid for"
}

variable "allowed_oauth_flows_user_pool_client" {
  type        = bool
  description = "Whether the client is allowed to follow the OAuth protocol whe interacting with Cognito User Pool (true or false)"
  default     = true
}

variable "allowed_oauth_scopes" {
  type        = list(string)
  description = "List of allowed OAuth scopes (phone, email, openid, profile, and aws.cognito, sign-in.user.admin)"
  default     = []
}

## user_pool_domain variables ##

variable "domain_name" {
  type        = string
  description = "The domain name"
}

## identity_provider variables ##

variable "provider_name" {
  type        = string
  description = "The provider name"
}

variable "provider_type" {
  type        = string
  description = "The provider type"
  default     = "SAML"
}

variable "attribute_mapping" {
  type        = map
  description = "The map of attribute mapping of user pool attributes."
  default     = {
    "name"    = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"
    "given_name" = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname"
    "family_name" = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname"
    "email" = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"
  }
}

## identity_pool variables ##

variable "saml_provider_name" {
  type        = string
  description = "Name of SAML provider"
}

variable "identity_pool_name_1" {
  type        = string
  description = "The cognito identity pool name"
}

variable "identity_pool_name_2" {
  type        = string
  description = "The cognito identity pool name"
}

variable "identity_pool_name_3" {
  type        = string
  description = "The cognito identity pool name"
}

variable "identity_pool_name_4" {
  type        = string
  description = "The cognito identity pool name"
}

variable "identity_pool_name_5" {
  type        = string
  description = "The cognito identity pool name"
}

variable "server_side_token_check" {
  type        = bool
  description = "Whether server-side token validation is enabled for the identity provider’s token or not.(true or false)"
  default     = false
}

variable "allow_unauthenticated_identities" {
  type        = bool
  description = "Required) - Whether the identity pool supports unauthenticated logins or not."
  default     = false
}